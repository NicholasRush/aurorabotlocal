#pragma once

#include <AuroraBot/DB.h>
#include <sleepy_discord/sleepy_discord.h>
#include <filesystem>
#include <sqlite_orm/sqlite_orm.h>

namespace AuroraBot {

    namespace Commands {

        const static std::string Prefix = "AB!";

        //Auto role
        const static std::string SetAutoRole = "set-auto-role";

            const static std::string SetAutoRoleUsage = SetAutoRole + " <Role ID>";

        const static std::string ClearAutoRole = "clear-auto-role";

            const static std::string ClearAutoRoleUsage = ClearAutoRole;

        //Sync
        const static std::string InitSync = "init-sync";

            const static std::string InitSyncUsage = InitSync;

        const static std::string StartSync = "start-sync";

            const static std::string StartSyncUsage = StartSync;

        const static std::string ConfirmSync = "confirm-sync";

            const static std::string ConfirmSyncUsage = ConfirmSync;

        const static std::string CancelSync = "cancel-sync";

            const static std::string CancelSyncUsage = CancelSync;

        const static std::string SetRelation = "set-relation";

            const static std::string SetRelationUsage = SetRelation + " <Relation ID>";

        //Custom channels
        const static std::string SetCustomCategory = "set-custom-category";

            const static std::string SetCustomCategoryUsage = SetCustomCategory + " <Category ID>";

        const static std::string ClearCustomCategory = "clear-custom-category";

            const static std::string ClearCustomCategoryUsage = ClearCustomCategory;

        const static std::string AddCustomChannel = "add-custom-channel";

            const static std::string AddCustomChannelUsage = AddCustomChannel + " <Type[voice/text]> <Name>";

        const static std::string DeleteCustomChannel = "delete-custom-channel";

            const static std::string DeleteCustomChannelUsage = DeleteCustomChannel + " <Channel ID>";

    }

    namespace Texts {

        const static std::string MustBeOwner = "This command is available only for server owner.";

        const static std::string Usage = "Usage: ";

        const static std::string WrongParameter = "Wrong parameter.";

        const static std::string Successful = "Successful.";

    }

    class AuroraBot : public SleepyDiscord::DiscordClient {

    private:

        std::unique_ptr<DB::DB> db;

        std::vector<DB::MemberLastRole> GetMemberLastRoles(std::string_view serverID, std::string_view userID);

    public:
        AuroraBot(std::string_view token, const std::filesystem::path &dbPath);

        void onReady(SleepyDiscord::Ready readyData) override;

        void onMember(SleepyDiscord::Snowflake<SleepyDiscord::Server> serverID, SleepyDiscord::ServerMember member) override;

        void onMessage(SleepyDiscord::Message message) override;

        void onDeleteServer (SleepyDiscord::UnavailableServer server) override;

        void onServer (SleepyDiscord::Server server) override;

        static std::vector<std::string> SplitCommand(std::string_view command);

        void onEditMember(SleepyDiscord::Snowflake<SleepyDiscord::Server> serverID, SleepyDiscord::User user, std::vector<SleepyDiscord::Snowflake<SleepyDiscord::Role>> roles, std::string nick) override;

    };

}