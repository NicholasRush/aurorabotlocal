#pragma once

#include <string>
#include <optional>
#include <sqlite_orm/sqlite_orm.h>
#include <filesystem>

namespace AuroraBot::DB {

    struct Server {

        std::string ID;

        std::optional<std::string> AutoRoleID;

        std::optional<std::string> CustomCategoryID;

    };

    struct CopyCommand {

        std::string UserID;

        std::string OriginServerID;

        std::optional<std::string> TargetServerID;

    };

    struct CopyCommandChannel {

        std::string UserID;

        std::string OriginChannelID;

        std::optional<std::string> TargetChannelID;

    };

    struct CopyCommandRole {

        std::string UserID;

        std::string OriginRoleID;

        std::optional<std::string> TargetRoleID;

    };

    struct MemberLastRole {

        std::string ServerID;

        std::string UserID;

        std::string RoleID;

    };

    struct CustomChannels {



    };

    inline auto GetDB(const std::filesystem::path &dbPath) {
        return sqlite_orm::make_storage(dbPath,
                sqlite_orm::make_table(
                    "Servers",
                    sqlite_orm::make_column("ID", &DB::Server::ID, sqlite_orm::primary_key()),
                    sqlite_orm::make_column("AutoRoleID", &DB::Server::AutoRoleID),
                    sqlite_orm::make_column("CustomCategoryID", &DB::Server::CustomCategoryID)
                ),
                sqlite_orm::make_table(
                    "CopyCommands",
                    sqlite_orm::make_column("UserID", &DB::CopyCommand::UserID, sqlite_orm::primary_key()),
                    sqlite_orm::make_column("OriginServerID", &DB::CopyCommand::OriginServerID),
                    sqlite_orm::make_column("TargetServerID", &DB::CopyCommand::TargetServerID),
                    sqlite_orm::foreign_key(&DB::CopyCommand::OriginServerID).references(&DB::Server::ID).on_update.cascade().on_delete.cascade(),
                    sqlite_orm::foreign_key(&DB::CopyCommand::TargetServerID).references(&DB::Server::ID).on_update.cascade().on_delete.cascade()
                ),
                sqlite_orm::make_table(
                    "CopyCommandsChannels",
                    sqlite_orm::make_column("UserID", &DB::CopyCommandChannel::UserID),
                    sqlite_orm::make_column("OriginChannelID", &DB::CopyCommandChannel::OriginChannelID),
                    sqlite_orm::make_column("TargetChannelID", &DB::CopyCommandChannel::TargetChannelID),
                    sqlite_orm::foreign_key(&DB::CopyCommandChannel::UserID).references(&DB::CopyCommand::UserID).on_update.cascade().on_delete.cascade()
                ),
                sqlite_orm::make_table(
                    "CopyCommandsRoles",
                    sqlite_orm::make_column("UserID", &DB::CopyCommandRole::UserID),
                    sqlite_orm::make_column("OriginRoleID", &DB::CopyCommandRole::OriginRoleID),
                    sqlite_orm::make_column("TargetRoleID", &DB::CopyCommandRole::TargetRoleID),
                    sqlite_orm::foreign_key(&DB::CopyCommandRole::UserID).references(&DB::CopyCommand::UserID).on_update.cascade().on_delete.cascade()
                ),
                sqlite_orm::make_table(
                    "MembersLastRoles",
                    sqlite_orm::make_column("ServerID", &DB::MemberLastRole::ServerID),
                    sqlite_orm::make_column("UserID", &DB::MemberLastRole::UserID),
                    sqlite_orm::make_column("RoleID", &DB::MemberLastRole::RoleID),
                    sqlite_orm::foreign_key(&DB::MemberLastRole::ServerID).references(&DB::Server::ID).on_update.cascade().on_delete.cascade()
                ));
    }

    using DB = decltype(GetDB(""));

}