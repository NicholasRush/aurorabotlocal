#include <AuroraBot/AuroraBot.h>

namespace AuroraBot {

    AuroraBot::AuroraBot(std::string_view token, const std::filesystem::path &dbPath) : SleepyDiscord::DiscordClient(std::string(token)){
        if (std::filesystem::exists(dbPath) && !std::filesystem::is_regular_file(dbPath)) {
            std::filesystem::path tempPath = dbPath;
            tempPath += "_" + std::to_string(std::time(nullptr));
            std::filesystem::rename(dbPath, tempPath);
        }
        db = std::make_unique<DB::DB>(DB::GetDB(dbPath));
        db->sync_schema(true);
    }

}