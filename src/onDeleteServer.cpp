#include <AuroraBot/AuroraBot.h>

namespace AuroraBot {

    void AuroraBot::onDeleteServer (SleepyDiscord::UnavailableServer server) {
        db->remove<DB::Server>(server.ID.string());
    }

}