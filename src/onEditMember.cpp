#include <AuroraBot/AuroraBot.h>

namespace AuroraBot {

    void AuroraBot::onEditMember(SleepyDiscord::Snowflake<SleepyDiscord::Server> serverID, SleepyDiscord::User user, std::vector<SleepyDiscord::Snowflake<SleepyDiscord::Role>> roles, std::string nick) {
        //Copy-past from onServer
        std::vector<DB::MemberLastRole> memberLastRoles = GetMemberLastRoles(serverID.string(), user.ID.string());
        std::vector<std::string> memberLastRolesToAdd;
        for (const SleepyDiscord::Snowflake<SleepyDiscord::Role> &memberRole : roles) {
            if (std::find_if(memberLastRoles.begin(), memberLastRoles.end(), [&](const DB::MemberLastRole &memberLastRole){
                return memberLastRole.RoleID == memberRole.string();
            }) == memberLastRoles.end()) {
                memberLastRolesToAdd.emplace_back(memberRole.string());
            }
        }
        std::vector<std::string> memberLastRolesToDelete;
        for (const DB::MemberLastRole &memberLastRole : memberLastRoles) {
            if (std::find_if(roles.begin(), roles.end(), [&](const SleepyDiscord::Snowflake<SleepyDiscord::Role> &memberRole){
                return memberRole.string() == memberLastRole.RoleID;
            }) == roles.end()) {
                memberLastRolesToDelete.emplace_back(memberLastRole.RoleID);
            }
        }
        db->remove_all<DB::MemberLastRole>(sqlite_orm::where(sqlite_orm::bitwise_and(sqlite_orm::bitwise_and(sqlite_orm::is_equal(&DB::MemberLastRole::ServerID, serverID.string()), sqlite_orm::is_equal(&DB::MemberLastRole::UserID, user.ID.string())), sqlite_orm::in(&DB::MemberLastRole::RoleID, memberLastRolesToDelete))));
        for (const std::string &memberLastRoleToAdd : memberLastRolesToAdd) {
            db->replace(DB::MemberLastRole{serverID.string(), user.ID.string(), memberLastRoleToAdd});
        }
    }

}