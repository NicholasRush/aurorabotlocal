#include <AuroraBot/AuroraBot.h>

namespace AuroraBot {

    void AuroraBot::onMessage(SleepyDiscord::Message message) {
        if (message.startsWith(Commands::Prefix + ' ')) {
            SleepyDiscord::Server server = getServer(message.serverID);
            std::vector<std::string> splitted = SplitCommand(message.content);

            if (splitted[1] == Commands::SetAutoRole) {
                if (server.ownerID == message.author.ID) {
                    if (splitted.size() == 3) {
                        bool found = false;
                        for (const SleepyDiscord::Role &role : server.roles) {
                            if (role.ID == splitted[2]) {
                                found = true;
                                break;
                            }
                        }
                        if (found) {
                            DB::Server dbServer = db->get<DB::Server>(server.ID.string());
                            dbServer.AutoRoleID = splitted[2];
                            db->update(dbServer);
                            sendMessage(message.channelID, Texts::Successful);
                        } else {
                            sendMessage(message.channelID, Texts::WrongParameter);
                        }
                    } else {
                        sendMessage(message.channelID, Texts::Usage + Commands::SetAutoRoleUsage + '.');
                    }
                } else {
                    sendMessage(message.channelID, Texts::MustBeOwner);
                }
            } else if (splitted[1] == Commands::ClearAutoRole) {
                if (server.ownerID == message.author.ID) {
                    if (splitted.size() == 2) {
                        DB::Server dbServer = db->get<DB::Server>(server.ID.string());
                        dbServer.AutoRoleID = std::nullopt;
                        db->update(dbServer);
                        sendMessage(message.channelID, Texts::Successful);
                    } else {
                        sendMessage(message.channelID, Texts::Usage + Commands::ClearAutoRoleUsage + '.');
                    }
                } else {
                    sendMessage(message.channelID, Texts::MustBeOwner);
                }
            }
        }
        /*if (message.startsWith(Prefix + ' ')) {
            SleepyDiscord::Server server = getServer(message.serverID);
            if (server.ownerID == message.author.ID) {
                std::vector<std::string> splitted = splitCommand(message.content);
                if (splitted[1] == Commands::setAutoRole) {
                    if (splitted.size() == 3) {
                        bool found = false;
                        for (const SleepyDiscord::Role &role : server.roles) {
                            if (role.ID == splitted[2]) {
                                found = true;
                                break;
                            }
                        }
                        if (found) {
                            SQLite::Statement query(db,
                                                    "INSERT INTO ServerConfig(ServerID, AutoRoleID) VALUES(:s, :ar) ON CONFLICT(ServerID) DO UPDATE SET AutoRoleID = :ar;");
                            query.bind(":ar", splitted[2]);
                            query.bind(":s", server.ID);
                            query.exec();
                            sendMessage(message.channelID, "Successful.");
                        } else {
                            sendMessage(message.channelID, "Error: wrong role ID.");
                        }
                    } else {
                        sendMessage(message.channelID, "Usage: " + Prefix + " " + Commands::setAutoRole + " <Role ID>.");
                    }
                } else if (splitted[1] == Commands::clearAutoRole) {
                    if (splitted.size() == 2) {
                        SQLite::Statement query(db,
                                                "UPDATE ServerConfig SET AutoRoleID IS NULL WHERE ServerID = :s;");
                        query.bind(":s", server.ID);
                        query.exec();
                        sendMessage(message.channelID, "Successful.");
                    } else {
                        sendMessage(message.channelID, "Usage: " + Prefix + " " + Commands::clearAutoRole + ".");
                    }
                } else if (splitted[1] == Commands::initSync) {
                    if (splitted.size() == 2) {
                        SQLite::Statement checkQuery(db, "SELECT true FROM CopyCommand WHERE UserID = :u;");
                        checkQuery.bind(":u", message.author.ID);
                        if (!checkQuery.executeStep()) {
                            SQLite::Statement query(db, "INSERT INTO CopyCommand(UserID, OriginServerID) VALUES(:u, :s);");
                            query.bind(":u", message.author.ID);
                            query.bind(":s", message.serverID);
                            query.exec();
                            sendMessage(message.channelID, "Sync initialized.");
                        } else {
                            sendMessage(message.channelID, "Error: you have not completed another sync.");
                        }
                    } else {
                        sendMessage(message.channelID, "Usage: " + Prefix + " " + Commands::initSync + ".");
                    }
                } else if (splitted[1] == Commands::applySync) {
                    if (splitted.size() == 2) {
                        SQLite::Statement checkQuery(db,
                                                     "SELECT OriginServerID, Applying FROM CopyCommand WHERE UserID = :u;");
                        checkQuery.bind(":u", message.author.ID);
                        if (checkQuery.executeStep()) {
                            if (checkQuery.getColumn("OriginServerID").getString() != message.serverID) {
                                if (checkQuery.getColumn("Applying").getInt() == 0) {
                                    SQLite::Statement applyQuery(db,
                                                                 "UPDATE CopyCommand SET TargetServerID = :s, Applying = true WHERE UserID = :u;");
                                    applyQuery.bind(":s", message.serverID);
                                    applyQuery.bind(":u", message.author.ID);
                                    applyQuery.exec();
                                    //SQLite::Statement insertRoleQuery(db, "INSERT INTO CopyRoles(UserID, OriginRoleID) VALUES(:u, :r);");
                                    bool first = true;
                                    for (const SleepyDiscord::Role &originRole: getRoles(
                                            checkQuery.getColumn("OriginServerID").getString()).vector()) {
                                        if (originRole.name == "Kurisu") {
                                            continue;
                                        }
                                        if (first) {
                                            sendMessage(message.channelID, "Sync applied. Set \"" +
                                                                           (originRole.name == "@everyone" ? "everyone"
                                                                                                           : originRole.name) +
                                                                           "\" role relation with " +
                                                                           Commands::setRelation + " command.");
                                            first = false;
                                        }
                                        SQLite::Statement insertRoleQuery(db,
                                                                          "INSERT INTO CopyRoles(UserID, OriginRoleID, OriginRoleName) VALUES(:u, :r, :rn);");
                                        insertRoleQuery.bind(":u", message.author.ID);
                                        insertRoleQuery.bind(":r", originRole.ID);
                                        insertRoleQuery.bind(":rn", (originRole.name == "@everyone" ? "everyone"
                                                                                                    : originRole.name));
                                        insertRoleQuery.exec();
                                        //insertRoleQuery.clearBindings();
                                    }
                                    //SQLite::Statement insertChannelQuery(db, "INSERT INTO CopyChannels(UserID, OriginChannelID) VALUES(:u, :c);");
                                    for (const SleepyDiscord::Channel &originChannel: getServerChannels(
                                            checkQuery.getColumn("OriginServerID").getString()).vector()) {
                                        if (originChannel.name.starts_with("__")) {
                                            continue;
                                        }
                                        SQLite::Statement insertChannelQuery(db,
                                                                             "INSERT INTO CopyChannels(UserID, OriginChannelID) VALUES(:u, :c);");
                                        insertChannelQuery.bind(":u", message.author.ID);
                                        insertChannelQuery.bind(":c", originChannel.ID);
                                        insertChannelQuery.exec();
                                        //insertChannelQuery.clearBindings();
                                    }
                                } else {
                                    sendMessage(message.channelID, "Error: sync already applied.");
                                }
                            } else {
                                sendMessage(message.channelID, "Error: you can't apply sync on origin server.");
                            }
                        } else {
                            sendMessage(message.channelID, "Error: there is no initialized sync to apply.");
                        }
                    } else {
                        sendMessage(message.channelID, "Usage: " + Prefix + " " + Commands::applySync + ".");
                    }
                } else if (splitted[1] == Commands::cancelSync) {
                    if (splitted.size() == 2) {
                        SQLite::Statement commandQuery(db, "DELETE FROM CopyCommand WHERE UserID = :u;");
                        commandQuery.bind(":u", message.author.ID);
                        commandQuery.exec();
                        sendMessage(message.channelID, "Successful.");
                    } else {
                        sendMessage(message.channelID, "Usage: " + Prefix + " " + Commands::cancelSync + ".");
                    }
                } else if (splitted[1] == Commands::setRelation) {
                    if (splitted.size() == 3) {
                        SQLite::Statement checkQuery(db,
                                                     "SELECT TargetServerID FROM CopyCommand WHERE UserID = :u and Applying = TRUE;");
                        checkQuery.bind(":u", message.author.ID);
                        if (checkQuery.executeStep()) {
                            if (checkQuery.getColumn("TargetServerID").getString() == message.serverID) {
                                SQLite::Statement getRoleQuery(db,
                                                               "SELECT OriginRoleID FROM CopyRoles WHERE UserID = :u and TargetRoleID IS NULL LIMIT 1;");
                                getRoleQuery.bind(":u", message.author.ID);
                                if (getRoleQuery.executeStep()) {
                                    bool found = false;
                                    for (const SleepyDiscord::Role &targetServerRole: getRoles(message.serverID).vector()) {
                                        if (targetServerRole.ID == splitted[2]) {
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (found) {
                                        SQLite::Statement updateQuery(db,
                                                                      "UPDATE CopyRoles SET TargetRoleID = :r WHERE UserID = :u and OriginRoleID = :or;");
                                        updateQuery.bind(":r", splitted[2]);
                                        updateQuery.bind(":u", message.author.ID);
                                        updateQuery.bind(":or", getRoleQuery.getColumn("OriginRoleID").getString());
                                        updateQuery.exec();
                                        SQLite::Statement getAnswerRoleQuery(db,
                                                                             "SELECT OriginRoleID, OriginRoleName FROM CopyRoles WHERE UserID = :u and TargetRoleID IS NULL LIMIT 1;");
                                        getAnswerRoleQuery.bind(":u", message.author.ID);
                                        if (getAnswerRoleQuery.executeStep()) {
                                            sendMessage(message.channelID, "Successful. Now set \"" +
                                                                           getAnswerRoleQuery.getColumn(
                                                                                   "OriginRoleName").getString() +
                                                                           "\" role relation with set-relation command.");
                                        } else {
                                            SQLite::Statement getAnswerChannelQuery(db,
                                                                                    "SELECT OriginChannelID FROM CopyChannels WHERE UserID = :u and TargetChannelID IS NULL LIMIT 1;");
                                            getAnswerChannelQuery.bind(":u", message.author.ID);
                                            if (getAnswerChannelQuery.executeStep()) {
                                                SleepyDiscord::Channel originChannel = getChannel(
                                                        getAnswerChannelQuery.getColumn("OriginChannelID").getString());
                                                sendMessage(message.channelID,
                                                            "Successful. Now set \"" + originChannel.name + "\" " +
                                                            (originChannel.type == SleepyDiscord::Channel::SERVER_CATEGORY
                                                             ? "category" : originChannel.type ==
                                                                            SleepyDiscord::Channel::SERVER_TEXT
                                                                            ? "text channel" : "voice channel") + " with " +
                                                            Commands::setRelation + " command.");
                                            } else {
                                                sendMessage(message.channelID,
                                                            "All relations configured. Now you can confirm sync with " +
                                                            Commands::confirmSync + " command.");
                                            }
                                        }
                                    } else {
                                        sendMessage(message.channelID, "Error: wrong role ID.");
                                    }
                                } else {
                                    SQLite::Statement getChannelQuery(db,
                                                                      "SELECT OriginChannelID FROM CopyChannels WHERE UserID = :u and TargetChannelID IS NULL LIMIT 1;");
                                    getChannelQuery.bind(":u", message.author.ID);
                                    if (getChannelQuery.executeStep()) {
                                        bool found = false;
                                        for (const SleepyDiscord::Channel &targetServerChannel: getServerChannels(
                                                message.serverID).vector()) {
                                            if (targetServerChannel.ID == splitted[2]) {
                                                found = true;
                                                break;
                                            }
                                        }
                                        if (found) {
                                            SQLite::Statement updateQuery(db,
                                                                          "UPDATE CopyChannels SET TargetChannelID = :c WHERE UserID = :u and OriginChannelID = :oc;");
                                            updateQuery.bind(":c", splitted[2]);
                                            updateQuery.bind(":u", message.author.ID);
                                            updateQuery.bind(":oc", getChannelQuery.getColumn("OriginChannelID").getString());
                                            updateQuery.exec();
                                            SQLite::Statement getAnswerChannelQuery(db,
                                                                                    "SELECT OriginChannelID FROM CopyChannels WHERE UserID = :u and TargetChannelID IS NULL LIMIT 1;");
                                            getAnswerChannelQuery.bind(":u", message.author.ID);
                                            if (getAnswerChannelQuery.executeStep()) {
                                                SleepyDiscord::Channel originChannel = getChannel(
                                                        getAnswerChannelQuery.getColumn("OriginChannelID").getString());
                                                sendMessage(message.channelID,
                                                            "Successful. Now set \"" + originChannel.name + "\" " +
                                                            (originChannel.type == SleepyDiscord::Channel::SERVER_CATEGORY
                                                             ? "category" : originChannel.type ==
                                                                            SleepyDiscord::Channel::SERVER_TEXT
                                                                            ? "text channel" : "voice channel") + " with " +
                                                            Commands::setRelation + " command.");
                                            } else {
                                                sendMessage(message.channelID,
                                                            "All relations configured. Now you can confirm sync with " +
                                                            Commands::confirmSync + " command.");
                                            }
                                        } else {
                                            sendMessage(message.channelID, "Error: wrong channel ID.");
                                        }
                                    } else {
                                        sendMessage(message.channelID, "Error: there is no relations need to be set.");
                                    }
                                }
                            } else {
                                sendMessage(message.channelID, "Error: you need to set relations on target server.");
                            }
                        } else {
                            sendMessage(message.channelID, "Error: there is no applying sync.");
                        }
                    } else {
                        sendMessage(message.channelID, "Usage: " + Prefix + " " + Commands::setRelation + " <ID>.");
                    }
                } else if (splitted[1] == Commands::confirmSync) {
                    if (splitted.size() == 2) {
                        SQLite::Statement checkQuery(db,"SELECT OriginServerID, TargetServerID FROM CopyCommand WHERE UserID = :u and Applying = TRUE LIMIT 1;");
                        checkQuery.bind(":u", message.author.ID);
                        if (checkQuery.executeStep()) {
                            if (checkQuery.getColumn("TargetServerID").getString() == message.serverID) {
                                SQLite::Statement rolesConfirmQuery(db, "SELECT true from CopyRoles where UserID = :u and TargetRoleID IS NULL LIMIT 1;");
                                rolesConfirmQuery.bind(":u", message.author.ID);
                                SQLite::Statement channelsConfirmQuery(db, "SELECT true from CopyChannels where UserID = :u and TargetChannelID IS NULL LIMIT 1;");
                                channelsConfirmQuery.bind(":u", message.author.ID);
                                if (!rolesConfirmQuery.executeStep() && !channelsConfirmQuery.executeStep()) {
                                    SQLite::Statement rolesQuery(db, "SELECT OriginRoleID, TargetRoleID FROM CopyRoles WHERE UserID = :u;");
                                    rolesQuery.bind(":u", message.author.ID);
                                    std::vector<SleepyDiscord::Role> originRoles = getRoles(checkQuery.getColumn("OriginServerID").getString());
                                    while (rolesQuery.executeStep()) {
                                        for (const SleepyDiscord::Role &originRole: originRoles) {
                                            if (originRole.ID == rolesQuery.getColumn("OriginRoleID").getString()) {
                                                editRole(checkQuery.getColumn("TargetServerID").getString(),rolesQuery.getColumn("TargetRoleID").getString(),originRole.name,originRole.permissions,originRole.color,originRole.hoist,originRole.mentionable);
                                                break;
                                            }
                                        }
                                    }
                                    SQLite::Statement channelsQuery(db, "SELECT OriginChannelID, TargetChannelID FROM CopyChannels WHERE UserID = :u;");
                                    channelsQuery.bind(":u", message.author.ID);
                                    while (channelsQuery.executeStep()) {
                                        SleepyDiscord::Channel originChannel = getChannel(channelsQuery.getColumn("OriginChannelID").getString());
                                        editChannel(channelsQuery.getColumn("TargetChannelID").getString(),originChannel.name,originChannel.topic);
                                        for (const SleepyDiscord::Overwrite &overwrite: originChannel.permissionOverwrites) {
                                            SQLite::Statement overwriteQuery(db, "SELECT TargetRoleID FROM CopyRoles WHERE UserID = :u and OriginRoleID = :or LIMIT 1;");
                                            overwriteQuery.bind(":u",message.author.ID);
                                            overwriteQuery.bind(":or",overwrite.ID);
                                            if (overwriteQuery.executeStep()) {
                                                editChannelPermissions(channelsQuery.getColumn("TargetChannelID").getString(),overwriteQuery.getColumn("TargetRoleID").getString(),overwrite.allow,overwrite.deny,overwrite.type);
                                            }
                                            //editChannelPermissions(channelsQuery.getColumn("TargetChannelID").getString(),overwrite.ID,overwrite.allow,overwrite.deny,overwrite.type);
                                        }
                                    }
                                    sendMessage(message.channelID, "Successful.");
                                } else {
                                    sendMessage(message.channelID, "Error: not all relations are configured.");
                                }
                            } else {
                                sendMessage(message.channelID, "Error: sync confirmation can be done only on target server.");
                            }
                        } else {
                            sendMessage(message.channelID, "Error: there is no applying sync.");
                        }
                    } else {
                        sendMessage(message.channelID, "Usage: " + Prefix + " " + Commands::confirmSync + ".");
                    }
                } else if (splitted[1] == "test") {
                    std::cout << "Roles:" << std::endl;
                    for (const SleepyDiscord::Role &role: server.roles) {
                        std::cout << "Name: " << role.name << std::endl;
                    }
                    std::cout << "Channels:" << std::endl;

                    for (const SleepyDiscord::Channel &channel: getServerChannels(message.serverID).vector()) {
                        std::cout << "Name: " << channel.name << ", type: "
                                  << (channel.type == SleepyDiscord::Channel::SERVER_CATEGORY ? "category" : channel.type ==
                                                                                                             SleepyDiscord::Channel::SERVER_TEXT
                                                                                                             ? "text channel"
                                                                                                             : "voice channel");
                        if (!channel.parentID.string().empty()) {
                            std::cout << ", parent: " << getChannel(channel.parentID).cast().name;
                        }
                        std::cout << std::endl;
                    }
                    std::cout << std::flush;
                } else {
                    sendMessage(message.channelID, "Error: wrong command.");
                }
            } else {
                sendMessage(message.channelID, "Error: only server owner can use this bot.");
            }
        }*/
    }

}